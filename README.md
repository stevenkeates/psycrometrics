# pSycrometrics

Compelation of basic psycrometric functions used to derive thermodynamic properties of air. Formulas are sourced from ASHRAE Fundamentals and reproduced as an R package to facilitate thermodynamic analysis of air-flow data collected to establish field performance of HVAC systems.

# License

Copyright (C) 2021  Steven Keates

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
